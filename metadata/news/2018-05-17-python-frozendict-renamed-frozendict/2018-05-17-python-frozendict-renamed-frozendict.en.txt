Title: dev-python/python-frozendict has been renamed to dev-python/frozendict
Author: Timo Gurr <tgurr@exherbo.org>
Content-Type: text/plain
Posted: 2018-05-17
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: dev-python/python-frozendict

Please install dev-python/frozendict and *afterwards* uninstall dev-python/python-frozendict.

1. Take note of any packages depending on dev-python/python-frozendict:
cave resolve \!dev-python/python-frozendict

2. Install dev-python/frozendict:
cave resolve dev-python/frozendict -x1

3. Re-install the packages from step 1.

4. Uninstall dev-python/python-frozendict
cave resolve \!dev-python/python-frozendict -x

Do it in *this* order or you'll potentially *break* your system.
