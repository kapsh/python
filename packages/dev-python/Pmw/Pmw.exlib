# Copyright 2009 Romain Iehl <riehl9@gmail.com>
# Copyright 2015 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'pmw-1.3.2-r1.ebuild' from Gentoo, which is:
#     Copyright 1999-2009 Gentoo Foundation

MY_PNV=${PN}-2.0.1

# TODO(somasis): upstream author if you're reading this I will fight you in real life if I ever meet you
require pypi [ pnv="${MY_PNV}" ]

case "${SLOT}" in
    1)  # python 2 library
        blacklist=3
    ;;
    2)  # python 3 library
        blacklist=2
    ;;
esac

require setup-py [  import='distutils' python_opts='[tk]' work="${MY_PNV}" \
                    blacklist="${blacklist}" ]

SUMMARY="A toolkit for building high-level compound widgets in Python using the Tkinter module."
HOMEPAGE="http://pmw.sourceforge.net/"

REMOTE_IDS="sourceforge:${PN}"

LICENCES="MIT"
PLATFORMS="~amd64"
MYOPTIONS="
    doc examples
"

DEPENDENCIES=""

# Tests are not unittests and show various GUIs. So we don't run them.
RESTRICT=test

install_one_multibuild() {
    setup-py_install_one_multibuild

    # they use separate versions of the library depending on python 2/3
    # the setup.py is smart enough to differenciate this with everything
    # but the docs are stored in different directories so we have to figure
    # it out on our own
    abi=$(python_get_abi)
    case ${SLOT} in
        1)
            dirname=${PN}_1_3_3
            dir="${WORK}"/PYTHON_ABIS/${abi}/${MY_PNV}/${PN}/${dirname}
        ;;
        2)
            dirname=${PN}_${PV//./_}
            dir="${WORK}"/PYTHON_ABIS/${abi}/${MY_PNV}/${PN}/${dirname}
        ;;
    esac

    # upstream installs docs and examples to library dir even though that's awful and they are too
    edo rm -rf "${IMAGE}"/$(python_get_sitedir)/${PN}/${dirname}/{doc,demos}

    if option doc; then
        dodoc "${dir}"/doc/*
    fi

    if option examples; then
        insinto "/usr/share/doc/${PNVR}/examples"
        doins "${dir}"/demos/*
    fi
}

